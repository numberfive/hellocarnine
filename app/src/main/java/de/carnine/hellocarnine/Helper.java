/*
 * Created by $user on 21.10.18 10:14
 * Copyright (c) 2018 . All rights reserved.
 * Last modified 21.10.18 09:01
 *
 */

package de.carnine.hellocarnine;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;

class Helper {
    private Context context;
    private WifiManager wifiManager;
    private WifiManager.WifiLock wifiLock;
    Helper(Context context){
        this.context = context;
    }

    boolean checkWifiOnAndConnected(){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connManager == null) {
            return false;
        }

        NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifiInfo != null && wifiInfo.isConnected()) {
            Log.d("Helper", "wifiInfo isConnected");
            wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if(wifiLock == null && wifiManager != null) {
                wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "CarNiNeWiFiLock");
                wifiLock.acquire();
            }
            return true;
        }

        NetworkInfo ethernetInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET );

        if (ethernetInfo != null && ethernetInfo.isConnected()) {
            Log.d("Helper", "ethernetInfo isConnected");
            return true;
        }

        return false;
    }

    InetAddress getBroadcastAddress() throws UnknownHostException, IllegalStateException {
        if(wifiManager == null){
            throw new IllegalStateException("please call checkWifiOnAndConnected first");
        }

        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
        // handle null somehow
        Log.d("Helper", "dhcpInfo Address " + intToIP(dhcpInfo.ipAddress));
        Log.d("Helper", "dhcpInfo Gateway " + intToIP(dhcpInfo.gateway));

        int broadcast = (dhcpInfo.ipAddress & dhcpInfo.netmask) | ~dhcpInfo.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }

    void shutdown(){
        Log.d("Helper", "shutdown");
        if(wifiLock != null) {
            wifiLock.release();
        }
        context = null;
    }

   static String intToIP(int ipAddress) {

       return String.format("%d.%d.%d.%d", (ipAddress & 0xff),
               (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
               (ipAddress >> 24 & 0xff));
    }
}
