/*
 * Created by $user on 27.10.18 09:58
 * Copyright (c) 2018 . All rights reserved.
 * Last modified 27.10.18 09:37
 *
 */

package de.carnine.hellocarnine;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

class TCPSSLConnector implements Runnable {
    private Context context;
    private TCPSSLConnectorEvents messageListener;
    private SSLSocket socket = null;
    private PrintWriter out = null;
    private String host;
    private boolean runThread = false;
    private int port;
    private Thread worker;

    TCPSSLConnector(TCPSSLConnectorEvents listener, Context context) {
        messageListener = listener;
        this.context = context;
    }

    public void connect(String host, int port) {
        this.host = host;
        this.port = port;
        worker = new Thread(this, "TCPSSLWorker");
        worker.start();
    }

    public void run() {
        try {
            runThread = true;

            //socket = (SSLSocket) socketFactory.createSocket();
            //socket.connect(new InetSocketAddress(host, port));
            socket = getConnection(host, port);
            Log.d("TCPSSLConnector", "Connecting...");
            socket.startHandshake();

            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),false);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            if(messageListener != null) {
                messageListener.connected();
            }

            while (runThread) {

                String serverMessage = in.readLine();

                if (serverMessage != null && messageListener != null) {
                    //call the method messageReceived from MyActivity class
                    messageListener.messageReceived(serverMessage);
                }

            }

            in.close();

        } catch (ConnectException e) {
            final String message = e.getMessage();
            if(messageListener != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable(){
                    @Override
                    public void run() {
                        messageListener.error(message);
                    }});

            }
        } catch (IOException e) {
            if(messageListener != null) {
                e.printStackTrace();
            }
        }
        finally {
            //the socket must be closed. It is not possible to reconnect to this socket
            // after it is closed, which means a new socket instance has to be created.
            try {
                if(socket != null ) {
                    socket.close();
                    socket = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(messageListener != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable(){
                    @Override
                    public void run() {
                        messageListener.stopped();
                    }});

            }
        }

        Log.d("TCPSSLConnector", "worker Thread done");
    }

    protected SSLSocket getConnection(String ip, int port) throws IOException  {
        try {
            KeyStore trustStore = KeyStore.getInstance("BKS");
            int bks_version = R.raw.server_v1;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                bks_version = R.raw.server; //The BKS file
            }
            InputStream trustStoreStream = context.getResources().openRawResource(bks_version);
            trustStore.load(trustStoreStream, "test".toCharArray());

            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
            SSLSocketFactory factory = sslContext.getSocketFactory();
            SSLSocket socket = (SSLSocket) factory.createSocket(ip, port);
            socket.setEnabledCipherSuites(getCipherSuitesWhiteList(socket.getEnabledCipherSuites()));
            return socket;
        } catch (GeneralSecurityException e) {
            Log.e(this.getClass().toString(), "Exception while creating context: ", e);
            throw new IOException("Could not connect to SSL Server", e);
        }
    }

    protected String[] getCipherSuitesWhiteList(String[] cipherSuites) {
        List<String> whiteList = new ArrayList<>();
        List<String> rejected = new ArrayList<>();
        for (String suite : cipherSuites) {
            String s = suite.toLowerCase();
            if (s.contains("anon") || //reject no anonymous
                    s.contains("export") || //reject no export
                    s.contains("null") || //reject no encryption
                    s.contains("md5") || //reject MD5 (weaknesses)
                    s.contains("_des") || //reject DES (key size too small)
                    s.contains("krb5") || //reject Kerberos: unlikely to be used
                    s.contains("ssl") || //reject ssl (only tls)
                    s.contains("empty")) {    //not sure what this one is
                rejected.add(suite);
            } else {
                whiteList.add(suite);
            }
        }
        Log.d(this.getClass().toString(), "Rejected Cipher Suites: {}" + rejected);
        return whiteList.toArray(new String[0]);
    }

    public void disconnect(boolean shutdown) {
        Log.d("TCPSSLConnector", "disconnect " + shutdown);
        runThread = false;

        SendAndCloseTask asyncClient = new SendAndCloseTask(out);
        asyncClient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "ByeBye");

        try {
            if(worker != null) {
                worker.join(3000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.d("TCPSSLConnector", "wait for worker done");
        worker = null;
        if(shutdown) {
            messageListener = null;
        }
        Log.d("TCPSSLConnector", "disconnect done");
    }

    public void sendHello(InetAddress adr) {
        SendHelloAsyncTask asyncClient = new SendHelloAsyncTask(messageListener);
        asyncClient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, adr);
    }

    public void sendMessage(String message){
        SendMessageTask asyncClient = new SendMessageTask(out);
        asyncClient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, message);
    }
}

interface TCPSSLConnectorEvents {
    void messageReceived(String message);
    void stopped();
    void connected();
    void error(String message);
    void answerHello(String message);
}

class SendMessageTask extends AsyncTask<String, Void, Void> {

    private PrintWriter out;
    SendMessageTask(PrintWriter out) {
        this.out = out;
    }

    @Override
    protected Void doInBackground(String... params) {

        if (out != null && !out.checkError()) {
            // send the messages
            for (String message : params) {
                out.println(message);
            }
            out.flush();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void nothing) {
        super.onPostExecute(nothing);
    }
}

class SendAndCloseTask extends AsyncTask<String, Void, Void> {

    private PrintWriter out;
    SendAndCloseTask(PrintWriter out) {
        this.out = out;
    }

    @Override
    protected Void doInBackground(String... params) {

        if (out != null && !out.checkError()) {
            // send the messages
            for (String message : params) {
                out.println(message);
            }
            out.flush();
            out.close();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void nothing) {
        super.onPostExecute(nothing);
    }
}

class SendHelloAsyncTask extends AsyncTask<InetAddress, String, String> {

    private TCPSSLConnectorEvents messageListener;
    SendHelloAsyncTask(TCPSSLConnectorEvents listener){
        messageListener = listener;
    }

    protected String doInBackground(InetAddress... params) {
        DatagramSocket socketBroadcast = null;
        String result = null;
        try {

            String messageStr = "\02Where is CarNiNe\03";
            byte[] sendData = messageStr.getBytes();
            //sendData[0] = 2;

            socketBroadcast = new DatagramSocket(6000);
            socketBroadcast.setBroadcast(true);
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, params[0], 6000);
            socketBroadcast.send(sendPacket);

            byte[] receiveData = new byte[512];
            DatagramPacket receivePacket = new DatagramPacket(receiveData,512);
            socketBroadcast.setSoTimeout(3000);
            socketBroadcast.receive(receivePacket);
            StringBuilder receivedText = new StringBuilder();
            String strReceived = new String(receivePacket.getData(), 0, receivePacket.getLength());

            Log.d("TCPSSLConnector", "sendHello response " + strReceived);

            receivePacket.setLength(512);
            socketBroadcast.receive(receivePacket);
            strReceived = new String(receivePacket.getData(), 0, receivePacket.getLength());
            Log.d("TCPSSLConnector", "sendHello response " + strReceived);
            receivedText.append(strReceived);
            while(!strReceived.contains("\03")) {
                receivePacket.setLength(512);
                socketBroadcast.receive(receivePacket);
                strReceived = new String(receivePacket.getData(), 0, receivePacket.getLength());
                Log.d("TCPSSLConnector", "sendHello response " + strReceived);
                receivedText.append(strReceived);
            }
            String message = receivedText.toString();
            Log.d("TCPSSLConnector", "sendHello response after loop " + message);

            if(message.startsWith("\02Here:")){
                result = message.replace("\02Here:", "");
                result = result.replace("\03", "");
            }

        } catch (SocketTimeoutException e) {
            Log.e("TCPSSLConnector", "Timeout wait Answer");
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socketBroadcast != null)
            {
                socketBroadcast.close();
            }
        }
        return result;
    }

    protected void onPostExecute(String result)
    {
        if(messageListener != null) {
            messageListener.answerHello(result);
        }
        super.onPostExecute(result);
    }
}