/*
 * Created by $user on 27.10.18 09:58
 * Copyright (c) 2018 . All rights reserved.
 * Last modified 27.10.18 09:53
 *
 */

package de.carnine.hellocarnine;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity implements TCPSSLConnectorEvents {
    private TCPSSLConnector connector;
    private Button btnConnect;
    private ProgressBar proWaiting;
    private Helper helper;
    private TextView textViewWelcome;
    private Handler handler;
    private boolean inShutdown;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        helper = new Helper(this);
        inShutdown = false;

        btnConnect = findViewById(R.id.btnConnect);
        proWaiting = findViewById(R.id.proWaiting);
        textViewWelcome = findViewById(R.id.textViewWelcome);
        proWaiting.setVisibility(View.GONE);
        btnConnect.setVisibility(View.VISIBLE);
        connector = new TCPSSLConnector(this, this);

        btnConnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                Log.d("MainActivity", "try Connect");
                btnConnect.setVisibility(View.GONE);
                proWaiting.setVisibility(View.VISIBLE);
            }
        });

        handler = new Handler(Looper.getMainLooper());

        if(!helper.checkWifiOnAndConnected()) {
            btnConnect.setVisibility(View.GONE);
            textViewWelcome.setText(R.string.NoWlanMessage);
        } else {
            try {
                final InetAddress adr =  helper.getBroadcastAddress();
                Log.d("MainActivity", "Broadcast to " + adr);
                handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            connector.sendHello(adr);
                        }
                    }, 1000);

            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*if (id == R.id.action_settings) {
            Intent i = new Intent(this, UserSettingsActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
            return true;
        }*/

        if (id == R.id.action_finish) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        Log.d("MainActivity", "Destroy");
        inShutdown = true;
        connector.disconnect(inShutdown);
        helper.shutdown();
        super.onDestroy();
    }

    public void messageReceived(String message) {
        Log.d("MainActivity", "messageReceived " + message);
        if(message.equals("Pass?")){
            connector.sendMessage("Pass:roboter");
        }

        if(message.equals("Pass:Ok")){
            btnConnect.setVisibility(View.GONE);
            proWaiting.setVisibility(View.GONE);
        }
    }

    public void connected() {
        Log.d("MainActivity", "connected");
        connector.sendMessage("Login");
    }

    public void stopped() {
        Log.d("MainActivity", "stopped");
        connector.disconnect(inShutdown);
        btnConnect.setVisibility(View.VISIBLE);
        proWaiting.setVisibility(View.GONE);

        if(!inShutdown) {
            final InetAddress adr;
            try {
                adr = helper.getBroadcastAddress();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        connector.sendHello(adr);
                    }
                }, 1000);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    public void error(String message) {
        Log.d("MainActivity", "error " + message);
    }

    public void answerHello(String message) {
        Log.d("MainActivity", "answerHello " + message);
        if(message == null){
            final InetAddress adr;
            try {
                adr = helper.getBroadcastAddress();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        connector.sendHello(adr);
                    }
                }, 100);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        } else {
            String[] parts = message.split(":");
            connector.connect(parts[0], Integer.parseInt(parts[1]));
            btnConnect.setVisibility(View.GONE);
            proWaiting.setVisibility(View.VISIBLE);
        }
    }
}
